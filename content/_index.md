+++
title = "Ansible Labs"
weight = 50
+++

# Ansible Labs and Workshops

On this web site we publish our Ansible labs and workshops we built for Red Hat events like Summit or AnsibleFest, or for other events. You can always find the latest version of a workshop by using the navigation bar on the left. We also copy workshops before applying major rewrites to the [archive](/archive/) section.
