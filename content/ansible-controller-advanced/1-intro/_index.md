+++
title = "Introduction to your lab"
weight = 1
+++

## About this Lab

You have already used Ansible Automation quite a bit and have started to look into the automation controller (formerly known as Ansible Tower, part of the Red Hat Ansible Automation Platform)?
Or you are already using automation controller? Cool.
We prepared this lab to give a hands-on introduction to some of the more advanced features of the automation controller. You’ll learn about:

- Using command line tools to manage automation controller
- Automation controller clustering
- Working with instance groups
- Ways to provide inventories (importing inventory, dynamic inventory)
- The smart inventory feature
- Optional: How to structure Ansible content in Git repositories
- Optional: How to work with the automation controller API

## So little time and so much to do…

{{% notice warning %}}
To be honest we got carried away slightly while trying to press all these cool features into a two-hours lab session. We decided to flag the last two chapters as "optional" instead of taking them out. If you find the time to run them, cool! If not, the lab guide will stay where it is, feel free to go through these lab tasks later (you don’t need an automation controller cluster for this).
{{% /notice %}}

## Want to Use this Lab after the event?

Definitely, the lab documentation is available here:

**[https://ansible-labs-crew.gitlab.io/](https://ansible-labs-crew.gitlab.io/)**

If you want to contribute to the workshop to make it better, file an issue in our [GitLab project](https://gitlab.com/ansible-labs-crew/ansible-labs-crew.gitlab.io/).

## Your Ansible Automation Platform Lab Environment

In this lab you work in a pre-configured lab environment. You will have
access to the following hosts:

| Role                         | URL for External Access (if applicable)  | Hostname Internal                    |
| ---------------------------- | ---------------------------------------- | ------------------------------------ |
| Automation controller node 1 | https://autoctl1.\<LABID>.\<SUBDOMAIN>.opentlc.com | autoctl1.\<LABID>.internal |
| Automation controller node 2 | https://autoctl2.\<LABID>.\<SUBDOMAIN>.opentlc.com | autoctl2.\<LABID>.internal |
| Automation controller node 3 | https://autoctl3.\<LABID>.\<SUBDOMAIN>.opentlc.com | autoctl3.\<LABID>.internal |
| Execution Node | | exec1.\<LABID>.internal |
| Private Automation Hub       | https://pah.\<LABID>.\<SUBDOMAIN>.opentlc.com       | pah.\<LABID>.internal      |
| Visual Code Web UI           | https://bastion.\<LABID>.\<SUBDOMAIN>.opentlc.com |  |
| Database Node                |  | pgdb.\<LABID>.internal |
| Managed RHEL 9 Host 1         |  | node1.\<LABID>.internal |
| Managed RHEL 9 Host 2         |  | node2.\<LABID>.internal |
| Managed RHEL 9 Host 3         |  | node3.\<LABID>.internal |

{{% notice tip %}}
The lab environments in this session have a **LABID** and a dedicated DNS **SUBDOMAIN**. You will be able to access the hosts using the external hostnames. Internally the hosts have different names as shown above. Follow the instructions given by the lab facilitators to receive the values for **LABID** and **SUBDOMAIN**.
{{% /notice %}}

{{% notice tip %}}
Red Hat Ansible Automation Platform (AAP) has already been installed and activated for you, the web UI will be reachable over HTTP/HTTPS.
{{% /notice %}}

{{% notice info %}}
In general, whenever you need a password, it's the same one provided for lab access.
{{% /notice %}}

As you can see the lab environment is pretty extensive. You basically have:

- A bastion host running the VSCode server.
- A four node automation platform cluster with a separate DB host, accessed via SSH or web UI. The cluster consists of:
  - Three hybrid nodes that make up the control plane and have execution abilities
  - one execution node
- A private automation hub instance
- Three managed RHEL 8 hosts

## Working the Lab

Some hints to get you started:

- Don’t type everything manually, use copy & paste from the browser when appropriate. But don’t stop to think and understand… ;-)
- To **edit files** or **open a terminal window**, we provide **VS Code**, basically the great VSCode Editor running in your browser. It's running on the bastion node and can be accessed through the URL **https://bastion.\<LABID>.\<SUBDOMAIN>.opentlc.com**.

{{% notice tip %}}
Commands you are supposed to run are shown with or without the expected output, whatever makes more sense in the context.
{{% /notice %}}

{{% notice tip %}}
The command line can wrap on the HTML page from time to time. Therefore the output is often separated from the command line for better readability by an empty line. **Anyway, the line you should actually run should be recognizable by the prompt.** :-)
{{% /notice %}}

## Accessing your Lab Environment

### Using VS Code

Your main point of contact with the lab is **VS Code**, providing a VSCode-experience in your browser.

Now open **VS Code** server using the **VS Code access** link from the lab landing page or use this link in your browser by replacing the **\<LABID\>** and **\<SUBDOMAIN>**:

```raw
https://bastion.<LABID>.<SUBDOMAIN>.opentlc.com
```

<!-- ![code-server login](../../images/vscode-pwd.png) -->

{{< figure src="../../images/vscode-pwd.png?width=30pc&classes=border,shadow" title="Click image to enlarge" >}}

Use the password **provided on the lab landing page** to login into the **VS Code** server web UI, you can close the **Welcome** tab. Now open a new terminal by heading to the menu item **Terminal** at the top of the page and select **New Terminal**. A new section will appear in the lower half of the screen and you will be greeted with a prompt:

<!-- ![code-server terminal](../../images/vscode-terminal.png) -->

{{< figure src="../../images/vscode-terminal.png?width=40pc&classes=border,shadow" title="Click image to enlarge" >}}

If unsure how to use **VS Code** server, read the [Visual Studio Code Server introduction](../../vscode-intro/), to learn more about how to create and edit files, and to work with the Terminal.

Congrats, you now have a shell terminal on your bastion node you can use to work on the command line. From here you run commands or access the other hosts in your lab environment if the lab task requires it.

### Direct Lab Access using SSH

You can of course use SSH directly to access the bastion node when you have an SSH client ready to go and know your way around:

```ssh
ssh lab-user@bastion.<LABID>.<SUBDOMAIN>.opentlc.com
```

The password is still the same.

Congrats, you now have a shell terminal on your bastion node. From here you run commands or access the other hosts in your lab environment if a lab task requires it.

{{% notice tip %}}
The user you are accessing the terminal as is `lab-user`, but your bastion node is setup to let you become `root` using _sudo_ without a password.
{{% /notice %}}

## Check and Configure `ansible-navigator`

Most prerequisite tasks have already been done for you:

- Ansible software is installed
- `sudo` has been configured on the managed hosts to run commands that require root privileges.

During this lab, you will always use `ansible-navigator` which supersedes the capabilities of `ansible-playbook`. Let's check Ansible Navigator has been installed correctly (your actual version might differ):

```bash
$ ansible-navigator --version
ansible-navigator 2.1.0
```

{{% notice note %}}
Your version of `ansible-navigator` might be different then the example above. Rest assured we keep testing and make sure these lab instructions work with the version shipped as part of the lab.
{{% /notice %}}

{{% notice warning %}}
In all subsequent exercises you should work as a regular user on the bastion node unless explicitly told otherwise.
{{% /notice %}}

### Create `ansible-navigator` configuration

Create a configuration file for `ansible-navigator`, otherwise you would have to add the parameters to every execution of `ansible-navigator`. Create the `~/.ansible-navigator.yml` file in your home directory. In your VSCode UI use either your preferred editor from the command line or the visual editor (create a new file by going to File, New File). Add the following content to your configuration file:

{{% notice warning %}}
Make sure you create the file as a dot-file! If you leave out the `.` prefix the file will only be read when `ansible-navigator` is executed in the same directory as the file and subsequent lab tasks will fail.
{{% /notice %}}

```yaml
---
ansible-navigator:
  execution-environment:
    image: pah.<LABID>.<SUBDOMAIN>.opentlc.com/ee-supported-rhel8:latest
    enabled: true
    container-engine: podman
    pull:
      policy: missing
    volume-mounts:
    - src: "/etc/ansible/"
      dest: "/etc/ansible/"
    environment-variables:
      pass:
      - CONTROLLER_HOST
      - CONTROLLER_USERNAME
      - CONTROLLER_PASSWORD
      - CONTROLLER_VERIFY_SSL
```

{{% notice warning %}}
Make sure to use the **LABID** and **SUBDOMAIN** provided by your instructor or the landing page.
{{% /notice %}}

Note the following parameters within the `ansible-navigator.yml` file:

- `execution-environment.image`: where the default execution environment is set, we switch it to your privat automation hub!
- `execution-environment.pull.policy`: set to **missing**, only download the execution environment if it doesn't already exist locally
- `execution-environment.environment-variables`: since execution environments are basically Linux containers which don't have access to your environment variables, we have to compile a list of variables we want to be sent into the container

For a full listing of every configurable knob checkout the [documentation](https://ansible-navigator.readthedocs.io/en/latest/settings/)

### Examining Execution Environments with ansible-navigator

Before we can use `ansible-navigator` we need to pull down an execution environment from our private automation hub. You find the details on how to log into your private automation hub on the same landing page where you found all the other lab access details.

First we need to log into our private automation hub.

```bash
podman login pah.<LABID>.<SUBDOMAIN>.opentlc.com
```

{{% notice note %}}
Note that you only have to use the Fully Qualified Domain Name of your private automation hub without the HTTPS:// part in front!
{{% /notice %}}

Run the `ansible-navigator` command with the `images` argument to look at execution environments configured on the control node:

```bash
ansible-navigator images
```

![ansible-navigator images](../../images/navigator-images.png)

{{% notice note %}}
The output you see might differ from the above output.
{{% /notice %}}

This command gives you information about all currently installed Execution Environments or EEs for short. Investigate an EE by pressing the corresponding number. For example pressing **0** with the above example will open the `ee-supported-rhel8` execution environment:

![ee main menu](../../images/navigator-ee-menu.png)

Selecting `2` for `Ansible version and collections` will show us all Ansible Collections installed on that particular EE, and the version of `ansible-core`:

![ee info](../../images/navigator-ee-collections.png)

To get back to the view before in `ansible-navigator` press **Esc**, if needed several times. The last press in the main menu will get you out of Navigator.

## Hint: Managed Nodes hostnames

As mentioned you can construct your internal hostnames with your **\<LABID>**. But there is an easier way: On your bastion host you can find an Ansible inventory file for your environment. Just look at it in your VSCode terminal and you'll get the internal hostnames:

```bash
cat /etc/ansible/hosts
```
