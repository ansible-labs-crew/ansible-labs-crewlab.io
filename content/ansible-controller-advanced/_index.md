+++
title = "Ansible automation controller advanced"
weight = 3
+++
## Ansible automation controller Advanced

- [Exercise 1 - Discover your lab](1-intro)

- [Exercise 2 - Introduction to automation controller clustering](2-clustering)

- [Exercise 3 - There is more to automation controller than the web UI](3-awx-collection-intro)

- [Exercise 4 - Primer on Execution Environments: Running Jobs in a Cluster](4-primer-executionenvs)

- [Exercise 5 - Controller Instance Groups](5-instance-groups)

- [Exercise 6 - Start parallel jobs across instances](6-parallel-jobs)

- [Exercise 7 - Automation Mesh](7-automation-mesh)

- [Exercise 8 - Advanced inventories](8-advanced-inventories)

- [Exercise 9 - OPTIONAL: Well structured content repositories](9-structured-content)

- [Exercise 10 - OPTIONAL: Discovering the automation controller API](10-rest-api)

- [Exercise 11 - OPTIONAL: Project Signing for Supply Chain Security](11-repo-sign)
