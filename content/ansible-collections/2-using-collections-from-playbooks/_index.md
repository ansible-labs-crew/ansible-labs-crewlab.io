+++
title = "Collections from Playbook"
weight = 2
+++

## What are Collections and why should I care?

Ansible Collections are a new distribution format for Ansible content that can include playbooks, roles, modules, and plugins. Modules are moved from the core Ansible repository into collections living in repositories outside of the core repository. This change in the content delivery process will allow Ansible to keep up the tremendous success and, coming with it, growth in content.

- Before collections, module creators had to wait for their modules to be included in an upcoming Ansible release or had to add them to roles, which made consumption and management more difficult.
- By distributing modules packaged in Ansible Content Collections along with roles, documentation and even playbooks, content creators are now able to move as fast or conservative as the technology they manage demands.

**Example**: A public cloud provider could make new functionality of an existing service available, that could be rolled out along with the ability to automate the new functionality with Ansible. With Ansible Collections the author doesn't have to wait for the next Ansible release and can instead roll out the new content independently. Prior to Ansible Collections the author had to wait for the next Ansible release.

For Ansible users, the benefit is that updated content can continuously be made available. Managing content this way also becomes easier as modules, plugins, roles, and docs that belong together are packaged together and versioned.

## A word on Execution Environments

With the architecture changes in the Red Hat Ansible Automation Platform 2 we introduced Execution Environments as a replacement for the old Python virtual environments. Execution Environments are basically standard Linux container images which provide everything you need to run your Playbook:

- Ansible Core

- Python and all the necessary Python dependencies

- Ansible Collections and their dependencies

- additional RPMs or other software packages

Execution Environments help us to run our Ansible Playbook reliably, reproducibly and predictably in any environment. We can build them with the new `ansible-builder` and push them to the automation hub. This lab is focusing on Ansible Collections which are a crucial building block for Execution Environments and we save the topic on how to build one for a future lab.

## Fully Qualified Collection Name

Ansible Collection names are a combination of two components. The first part is the name of the author who wrote and maintains the Ansible Collection. The second part is the name of the Ansible Collection. This allows one author to have multiple Collections. It also allows multiple authors to have Ansible Collections with the same name.

```yaml
<author>.<collection>
```

These are examples for Ansible Collection names:

- ansible.posix

- geerlingguy.k8s

- theforeman.foreman

To identify a specific module in an Ansible Collection, we add the name of it as the third part:

```yaml
<author>.<collection>.<module>
```

Valid examples for a fully qualified Ansible Collection Name:

- ansible.posix.selinux

- geerlingguy.k8s.kubernetes

- theforeman.foreman.user

## Working with Ansible Collections

For the following exercise, we will use a collection written by the Ansible Core Team. The name of the author is therefore "ansible". You can find a list of all modules and collections written by the Ansible Core Team on [Ansible Galaxy](https://galaxy.ansible.com/ansible). Head over there and have a good look around!

As you can see they maintain several collections and roles. One of their collections is called "posix" and we can find the documentation and additional details on the [Ansible Galaxy POSIX Collection](https://galaxy.ansible.com/ansible/posix) page.

One of the modules provided by this collection allows us to manage [SELinux](https://www.redhat.com/en/topics/linux/what-is-selinux) settings. The fully qualified collection name for this module is therefore `ansible.posix.selinux`.

You can find more details about [using collections](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html) in the [Ansible Documentation](https://docs.ansible.com/).

## Create your project directory

Before we get started, we need to setup the basic structure for our exercise.

Bring up a browser window with **VS Code** and open a terminal (Click on **Terminal** in the menu bar and then **New Terminal**). In the terminal run:

```bash
[student@ansible-1 ~]$ cd
[student@ansible-1 ~]$ mkdir collections-lab
[student@ansible-1 ~]$ cd collections-lab
```

Unless told otherwise, all your playbooks and other files should be created inside this directory.

## Install the Ansible Collection

The `ansible.posix.selinux` module which we want to use for this exercise, is part of the `ansible.posix` collection. We have to install this collection first, before we can use its modules. The `ansible-galaxy` command line tool can be used to automate the installation. It is preconfigured to search for roles and collections on [Ansible Galaxy](https://galaxy.ansible.com/) so we can just specify the collection name and it will take care of the rest.

When you install a collection with `ansible-galaxy`, they are installed in your local `~/.ansible` directory. This can be overwritten by using the `-p /path/to/collection` switch. `ansible-navigator` by default will use the collections which are part of the execution environment in use. To make development and testing easy and convenient, `ansible-navigator` automatically respects the content of an `collections` directory, if it exists. Therefore, in the following command, we have to install our collection into the `collections` sub directory to make sure `ansible-navigator` will pick it up and **not** in the default location..

```bash
[student@ansible-1 collections-lab]$ ansible-galaxy collection install ansible.posix -p collections
```

{{% notice note %}}
You will notice the `ansible-galaxy` command will automatically create the `collections` sub directory, and you don't have to do it yourself.
{{% /notice %}}

This will install the collection on your system, only if it wasn't installed before. To force the installation, for example to make sure you're on the latest version, you can add the force switch `-f`.

```bash
[student@ansible-1 collections-lab]$ ansible-galaxy collection install -f ansible.posix -p collections
```

This will always download and install the latest version, even if it was already up to date. Ansible Collections can have dependencies for other Ansible Collections as well - if you want to make sure those dependencies are refreshed as well, you can use the `--force-with-deps` switch.

In recent versions `ansible-galaxy` knows a command to list installed collections:

```bash
student@ansible-1 collections-lab]$ ansible-galaxy collection list -p collections/
```

will list the collection installed to the specified directory.

## Browse the Documentation

The `ansible-navigator` command has a mode to read documentation as well. It will use the same priority logic by looking into the execution environment first, and then your local `collections` directory second. In the interactive mode it's easy to browse through all the currently installed collections and dig into their roles, modules and other plugins.

Let's have a look at the module documentation for the `selinux` module of the `ansible.posix` collection, which we are going to use in the next part of the exercise:

```bash
[student@ansible-1 collections-lab]$ ansible-navigator doc ansible.posix.selinux
```

{{% notice note %}}
You leave the document viewer by pressing "Esc".
{{% /notice %}}

## Write an Ansible Playbook

We want to use the SELinux module to make sure it is configured in enforcing mode. SELinux is a kernel feature which brings extra security to our Linux system and it is highly recommended to always keep it enabled and in enforcing mode. If you're new to SELinux, there is a nice article on [What is SELinux](https://www.redhat.com/en/topics/linux/what-is-selinux) to get you started.

Let's write a simple playbook which enables SELinux and sets it to enforcing mode on the local machine. In this lab you can use the visual **VS Code** editor or run an editor of your choice from the terminal. Create the Playbook `enforce-selinux.yml` with the following content:

```yaml
---
- name: set SELinux to enforcing
  hosts: node1
  become: yes
  tasks:
    - name: install dependencies
      ansible.builtin.yum:
        name: python3-libselinux
        state: present
    - name: set SElinux to enforcing
      ansible.posix.selinux:
        policy: targeted
        state: enforcing
```

Make sure you save the playbook as `enforce-selinux.yml` in your student users `collections-lab` directory.

{{% notice note %}}
Pay special attention to the module name. Typically you would see something like `selinux`, but since we are using a module provided by an Ansible Collection, we have to specify the fully qualified collection name.
{{% /notice %}}

## Test the playbook

Now let's run the Playbook and see what happens:

```bash
[student@ansible-1 collections-lab]$ ansible-navigator run enforce-selinux.yml -m stdout
```

We can use different modi when working with `ansible-navigator`. By default the **interactive** mode which allows us to navigate and dig into specific details is used. However, if we want to share the playbook output, the **stdout** mode might be more useful. You can add the mode with the `-m` command line switch like you just did.

In **stdout** mode the output should look like this:

```raw
PLAY [set SELinux to enforcing] ************************************************

TASK [Gathering Facts] *********************************************************
ok: [node1]

TASK [install dependencies] ****************************************************
ok: [node1]

TASK [set SElinux to enforcing] ************************************************
ok: [node1]

PLAY RECAP *********************************************************************
node1 : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

If SELinux was not set to enforcing mode before, you might see "changed" instead of "ok". If it did say "changed" and you run it a second time, you should now see "ok" - the magic of [Ansible idempotency](https://docs.ansible.com/ansible/latest/reference_appendices/glossary.html).

## Define dependencies

Since our playbook will fail to work without the collection installed, we want to document this dependency. This can be easily done by creating a `collections/requirements.yml` file. As you should expect by now, the syntax of that file is of course YAML and very simple.

{{% notice note %}}
Make sure you save this file in the correct folder. The full path and file name is `~/collections-lab/collections/requirements.yml`.
{{% /notice %}}

After the obligatory YAML header '---' we have a dictionary variable called `collections`:

```yaml
---
collections:
  - ansible.posix
```

You can also extend that by specifying a minimum version:

```yaml
---
collections:
  - name: "ansible.posix"
    version: ">1"
```

Or you specify a specific version:

```yaml
---
collections:
  - name: "ansible.posix"
    version: "1.4.0"
```

With the same syntax you can of course add all the collections you need to run your playbook. Tools like `ansible-galaxy` and the Red Hat Ansible Platform automation controller can parse this file and install the collections for you.

```bash
[student@ansible-1 collections-lab]$ ansible-galaxy collection install -r collections/requirements.yml -p collections
```

{{% notice note %}}
Because the `ansible.posix` collection was already installed by you, it won't install again!
{{% /notice %}}

The automation controller of the Red Hat Ansible Automation Platform will automatically look for this file and install the collections listed. Automation controller is the rock star previously known as Ansible Tower, before its architecture was modernized in the Red Hat Ansible Automation Platform 2.
