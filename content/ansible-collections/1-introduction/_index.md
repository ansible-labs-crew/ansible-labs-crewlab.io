+++
title = "Introduction"
weight = 1
+++

This chapter will introduce you to your lab environment and introduce you to the importance of Ansible Collections, what they are and where existing collections can be found and consumed.

## Your Lab Environment

These first few lab exercises will be exploring the command-line utilities of the Ansible Automation Platform.  This includes

- [ansible-navigator](https://github.com/ansible/ansible-navigator) - a command line utility and text-based user interface (TUI) for running and developing Ansible automation content.
- [ansible-core](https://docs.ansible.com/core.html) - the base executable that provides the framework, language and functions that underpin the Ansible Automation Platform.  It also includes various cli tools like `ansible`, `ansible-playbook` and `ansible-doc`.  Ansible Core acts as the bridge between the upstream community with the free and open source Ansible and connects it to the downstream enterprise automation offering from Red Hat, the Ansible Automation Platform.
- [Execution Environments](https://docs.ansible.com/automation-controller/latest/html/userguide/execution_environments.html) - not specifically covered in this workshop because the built-in Ansible Execution Environments already included all the Red Hat supported collections which includes all the collections we use for this workshop.  Execution Environments are container images that can be utilized as Ansible execution.

To access your individual lab environment, your instructor will share an lab specific URL with you. This will bring you to a Workshop page with all the access details. The page will look something like this (the URL's will be different, of course):

![landing page](../../images/lab-information.png)

In this lab you work in a pre-configured lab environment. You will have access to the following hosts:

| Role | inventory name | external URL |
| --- | --- | --- |
| Automation Controller | ansible-1 | https://student\<N>.\<LABID>.example.opentlc.com |
| Private Automation Hub | - | https://hub.\<LABID>.example.opentlc.com |
| Managed Host 1 | node1 | - |
| Managed Host 2 | node2 | - |
| Managed Host 3 | node3 | - |

{{% notice note %}}
The lab environments in this session have a **\<LABID>** and are separated by numbered **student\<N>** accounts. Follow the instructions given by the lab facilitators to receive the values for **student\<N>** and **\<LABID>**!
{{% /notice %}}

## Accessing your Lab Environment

Your main points of contact with the lab is **VS Code**, providing a VSCode-experience in your browser.

Now open **VS Code** server using the **VS Code access** link from the lab landing page or use this link in your browser by replacing **\<N\>** by your student number and the **\<LABID\>**:

```bash
     https://student<N>-code.<LABID>.example.opentlc.com
```

![code-server login](../../images/vscode-pwd.png)

Use the password **provided on the lab landing page** to login into the **VS Code** server web UI, you can close the **Welcome** tab. Now open a new terminal by heading to the menu item **Terminal** at the top of the page and select **New Terminal**. A new section will appear in the lower half of the screen and you will be greeted with a prompt:

![code-server terminal](../../images/vscode-terminal.png)

If unsure how to use **VS Code** server, read the [Visual Studio Code Server introduction](../../vscode-intro/), to learn more about how to create and edit files, and to work with the Terminal.

Congrats, you now have a shell terminal on your Automation Controller node you can use to work on the command line. From here you run commands or access the other hosts in your lab environment if the lab task requires it.

Now in the terminal become root:

```bash
[student@ansible-1 ~]$ sudo -i
```

Most prerequisite tasks have already been done for you:

- Ansible software is installed

- during this lab, we will always use `ansible-navigator` which supersedes the capabilities of `ansible-playbook`

- `sudo` has been configured on the managed hosts to run commands that require root privileges.

Check Ansible has been installed correctly (your actual Ansible version might differ):

```bash
[root@ansible-1 ~]# ansible-navigator --version
ansible-navigator 1.1.0
```

{{% notice note %}}
Your version of `ansible-navigator` might be different then the example above. Rest assured we keep testing and make sure these lab instructions work with the version shipped as part of the lab.
{{% /notice %}}

Log out of the root account again:

```bash
[root@ansible-1 ~]# exit
logout
[student@ansible-1 ~]$
```

{{% notice warning %}}
In all subsequent exercises you should work as the student user on the Automation Controller node if not explicitly told differently.
{{% /notice %}}

## Examining Execution Environments with ansible-navigator

Run the `ansible-navigator` command with the `images` argument to look at execution environments configured on the control node:

```bash
[student@ansible-1 ~]$ ansible-navigator images
```

![ansible-navigator images](../../images/navigator-images.png)

{{% notice note %}}
The output you see might differ from the above output.
{{% /notice %}}

This command gives you information about all currently installed Execution Environments or EEs for short.  Investigate an EE by pressing the corresponding number.  For example pressing **2** with the above example will open the `ee-supported-rhel8` execution environment:

![ee main menu](../../images/navigator-ee-menu.png)

Selecting `2` for `Ansible version and collections` will show us all Ansible Collections installed on that particular EE, and the version of `ansible-core`:

![ee info](../../images/navigator-ee-collections.png)

## Examining the ansible-navigator configuration

Either use Visual Studio Code to open or use the `cat` command to view the contents of the `ansible-navigator.yml` file.  The file is located in the home directory:

```bash
[student@ansible-1 ~]$ cat ~/.ansible-navigator.yml
---
ansible-navigator:
  ansible:
    inventories:
    - /home/student<X>/lab_inventory/hosts

  execution-environment:
    image: registry.redhat.io/ansible-automation-platform-20-early-access/ee-supported-rhel8:2.0.0
    enabled: true
    container-engine: podman
    pull-policy: missing
    volume-mounts:
    - src: "/etc/ansible/"
      dest: "/etc/ansible/"
```

Note the following parameters within the `ansible-navigator.yml` file:

- `inventories`: shows the location of the ansible inventory being used

- `execution-environment`: where the default execution environment is set

For a full listing of every configurable knob checkout the [documentation](https://ansible-navigator.readthedocs.io/en/latest/settings/)
