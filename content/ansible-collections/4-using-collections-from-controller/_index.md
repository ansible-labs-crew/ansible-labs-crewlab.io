+++
title = "Collections in automation controller"
weight = 4
+++

The automation controller, which is the successor of Ansible Tower, supports Ansible Collections starting with Tower version 3.5 - earlier versions will not automatically install and configure them for you. To make sure Ansible Collections are recognized by the automation controller a requirements file is needed and has to be stored in the proper directory.

Ansible Galaxy is already configured by default, however if you want your automation controller to prefer and fetch supported content from the Red Hat Automation Hub, additional configuration changes are required. They are addressed in a the chapter [Use Automation Hub](../6-automation-hub-and-galaxy/) in this lab.

In this exercise you will learn how to define an Ansible Collection as a requirement in a format recognized by automation controller. We'll be using the `ansible.posix` collection again from Ansible Galaxy. As you probably know for automation controller to access the needed bits and pieces a version control system is needed. For the sake of keeping this lab setup easy, we'll set up a local Git server for this.

## Set up a Local Git Server

So, let’s get started. We have to create a simplistic Git server on our control host. Typically you would work with GitHub, GitLab, Gitea, or any other Git server.

{{% notice warning %}}
Make sure to run these steps from your users home directory!
{{% /notice %}}

```bash
[student@ansible-1 ~]$ cd
[student@ansible-1 ~]$ wget https://gitlab.com/ansible-labs-crew/ansible-labs-crew.gitlab.io/-/raw/main/content/ansible-collections/4-using-collections-from-controller/simple_git.yml
[student@ansible-1 ~]$ ansible-navigator run simple_git.yml -e "git_project=controller_collections"
```

Next we will clone the repository on the control host. To enable you to work with git on the command line the SSH key for user *student* was already added to the Git user *git*. Next, clone the repository on the control machine:

```bash
[student@ansible-1 ~]$ git clone git@ansible-1:projects/controller_collections
# Message "warning: You appear to have cloned an empty repository." is OK and can be ignored
[student@ansible-1 ~]$ git config --global push.default simple
[student@ansible-1 ~]$ git config --global user.name "Your Name"
[student@ansible-1 ~]$ git config --global user.email you@example.com
[student@ansible-1 ~]$ cd controller_collections/
```

{{% notice tip %}}
The repository is currently empty. The three config commands are just there to avoid warnings from Git.
{{% /notice %}}

You now have a local Git server that can be accessed via SSH from automation controller.

## Create content for the automation controller project

Automation controller can download and install Ansible Collections automatically before executing a Job Template. If a `collections/requirements.yml` exists in your project, it will be parsed and the Ansible Collections specified in this file will be *automatically installed*.

The format of the `requirements.yml` for Ansible Collections is very similar to the one for roles, however it is very important to store in the folder `collections`.

Let's create the files needed to see how you can use collections in automation controller. This is of course just a simple example. First create the `collections` directory in your Git repo (you should have changed into `controller_collections` already above):

```bash
[student@ansible-1 controller_collections]$ mkdir collections
```

Then create the `requirements.yml` file listing the collection(s) you need:

{{% notice warning %}}
Make sure to create the `requirements.yml` file **in** the `collections` folder!
{{% /notice %}}

```yaml
---
collections:
- ansible.posix
```

As this is a simple example we'll just add a Playbook to the Git repo now. Normally you would have a lot more content in your project repository. So what could we do as an example instead of using the `ansible.posix` collection again? Let's create a Playbook to configure an `at` job:

```yaml
---
- name: Install "at" Job
  hosts: all

  tasks:
  - name: at command needs to be installed
    yum:
      name: at
      state: present

  - name: Schedule a command to execute in 20 minutes as root
    ansible.posix.at:
      command: ls -d / >/dev/null
      count: 20
      units: minutes
```

Save the file as `configure_at_job.yml` in the **controller_collections** folder.

{{% notice tip %}}
Note the usage of the Fully Qualified Collection Name in the Playbook
{{% /notice %}}

Make sure everything looks fine:

```bash
[student@ansible-1 controller_collections]$ tree
.
├── collections
│   └── requirements.yml
└── configure_at_job.yml
```

So far you created the code only locally on the control host, now you are ready to add it to the repository and push it:

```bash
[student@ansible-1 controller_collections]$ git add collections configure_at_job.yml
[student@ansible-1 controller_collections]$ git commit -m "Adding requirements.yml and playbook"
[student@ansible-1 controller_collections]$ git push
```

## Create the Project and Job Template

Now it's time to access your automation controller web UI if you haven't done so out of curiosity already. Point your browser to the URL you were given on the lab landing page, similar to `https://student<N>.<LABID>.example.opentlc.com` (replace `<N>` with your student number and `<LABID>` with the ID of this lab) and log in as `admin`. You can find the password again on the lab landing page.

To run your new Playbook in automation controller you have to configure a number of objects:

- An **Inventory** with the managed hosts
- **Machine Credentials** to access the managed hosts
- Git **Credentials** to access your Git repository via SSH
- The Git repo as **Project**
- A job **Template** to run the Playbook

We'll be a bit verbose for students new to automation controller. If you are an automation controller old-hand, just skip through and finish the configuration steps shown.

## Inventory and Machine Credentials

The inventory **Workshop Inventory** and the machine credentials **Workshop Credentials** have already been created in your lab environment.

## Configure SCM Credentials

Now we will configure the credentials to access the Git repo on your controller host via SSH. In the **Resources** menu choose **Credentials**. Now:

Click the ![add](../../images/blue_add.png?classes=inline) button to add new credentials

- **Name:** Git Credentials
- **Organization:** Click on the magnifying glass, pick **Default** and click **Select**
- **Credential Type:** Click on the magnifying glass, pick **Source Control** as type (you may have to use the search or cycle through the types to find it).
- **Username:** git

As we are using SSH key authentication, you have to provide an SSH private key that can be used to access the host with the Git repo as the user `git`.

{{% notice tip %}}
The Playbook we used to configure Git added the SSH public key to the `authorized_keys` of user `git`
{{% /notice %}}

Bring up your **VS Code** server terminal on Tower and use `cat` to get the SSH private key:

```bash
[student@ansible-1 ~]$ cat ~/.ssh/id_rsa
-----BEGIN RSA PRIVATE KEY----
MIIEpAIBAAKCAQEA2nnL3m5sKvoSy37OZ8DQCTjTIPVmCJt/M02KgDt53+baYAFu1TIkC3Yk+HK1
[...]
-----END RSA PRIVATE KEY-----
```

- Copy the **complete private key** (including "BEGIN" and "END" lines) from the output and paste it into the **SCM Private Key** field in the web UI.

- Click **Save**

You have now setup credentials to access the Git repo on your control host.

## Set up the Project

It's time to set up the automation controller **Project** pointing to your Git repository holding the Playbook and collections requirements file.

- Go to **Resources ⇒ Projects** in the side menu view click the ![add](../../images/blue_add.png?classes=inline) button. Fill in the form:
- **Name:** Collections Repo
- **Organization:** Default
- **Source Control Credential Type:** Git

Now you need the URL to access the repo. Enter the URL into the Project configuration:

- **Source Control URL:** git@ansible-1:projects/controller_collections
- **Source Control Credential:** Click the magnifying glass and choose `Git Credentials`, click **Select**
- **Options:** Check **Clean** and **Update Revision on launch**
- Click **Save**

The new Project will be synced automatically after creation. If everything went fine, you should see a green icon to the left of the new Project.

## Create the Job Template and run it

The last step is to create a job **Template** to run the Playbook. Go to the **Templates** view, click the ![add](../../images/blue_add.png?classes=inline) button and choose **Add job template**.

- **Name:** Install AT Job
- **Job Type:** Run
- **Inventory:** Workshop Inventory (you will have to click on the magnifying glass)
- **Project:** Collections Repo (you will have to click on the magnifying glass)
- **Playbook:** `configure_at_job.yml` from the dropdown list
- **Credentials:** Workshop Credentials (you will have to click on the magnifying glass)

- We need to run the tasks as root so check **Privilege Escalation**

- Click **Save**

You can start the job by directly clicking the blue **Launch** button, or by clicking on the rocket in the Job Templates overview. After launching the Job Template, you are automatically brought to the job overview where you can follow the playbook execution in real time.

After the Job has finished bring up your VSCode terminal, as the job was run on all three managed nodes and the automation controller node, you can simply check the result here. Run `sudo at -l` and you should see your job was scheduled successfully.

## Troubleshooting

Since Red Hat automation controller does only check for updates in the repository in which you stored your Playbook, it might not do a refresh if there was a change in the Ansible Collection used by your Playbook. This happens particularly if you also combine Roles and Collections.

In this case you should check the option **Delete on Update** which will delete the entire local directory during a refresh.

If there is a problem while parsing your `requirements.yml` it's worth testing it with the `ansible-galaxy` command. As a reminder, Red Hat automation controller basically also just runs the command for you with the appropriate parameters, so testing this works manually makes a lot of sense.

```bash
ansible-galaxy collection install -r collections/requirements.yml -f
```

{{% notice tip %}}
The `-f` switch will forces a fresh installation of the specified Ansible Collections, otherwise `ansible-galaxy` will only install it, if it wasn't already installed. You can also use the `--force-with-deps` switch to make sure Ansible Collections which have dependencies to others are refreshed as well.
{{% /notice %}}
