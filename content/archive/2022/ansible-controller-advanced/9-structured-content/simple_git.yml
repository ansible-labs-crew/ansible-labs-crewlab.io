---
- name: Create a simple SSH-based Git server
  hosts: bastion.<LABID>.internal
  become: true
  gather_facts: false

  vars:
    git_user: git
    projects_dir: projects
    git_project: structured-content
    username: "lab-user"
    reference_user: lab-user

  tasks:
    - name: generate public key
      ansible.builtin.shell:
        cmd: ssh-keygen -y -f /home/lab-user/.ssh/<LABID>key.pem -N "" > /home/lab-user/.ssh/id_rsa.pub
        creates: /home/lab-user/.ssh/id_rsa.pub

    - name: fix permissions of public key
      ansible.builtin.file:
        path: /home/lab-user/.ssh/id_rsa.pub
        mode: 0644
        owner: lab-user
        group: users

    - name: slurp the reference user's SSH public key
      ansible.builtin.slurp:
        src: /home/{{ reference_user }}/.ssh/id_rsa.pub
      register: git_authorized_key

    - name: install git and tree
      ansible.builtin.dnf:
        name:
          - tree
          - git
        state: latest

    - name: make sure git-shell is in /etc/shells
      ansible.builtin.lineinfile:
        dest: /etc/shells
        line: /usr/bin/git-shell

    - name: git user '{{ git_user }}' exists and uses git-shell
      ansible.builtin.user:
        name: "{{ git_user }}"
        comment: "Git server user"
        shell: /bin/bash

    - name: add the slurped authorized keys to the git user
      ansible.builtin.authorized_key:
        user: "{{ git_user }}"
        state: present
        key: "{{ git_authorized_key['content'] | b64decode }}"

    - name: define projects directory variable
      ansible.builtin.set_fact:
        projects_dir: "/home/{{ git_user }}/projects"

    - name: Ensure projects directory exists and has the correct access rights
      ansible.builtin.file:
        path: "{{ projects_dir }}"
        owner: "{{ git_user }}"
        group: "{{ git_user }}"
        recurse: true
        mode: ug+rwX
        state: directory

      # If the directory exists, we don't want to blow away its
      # contents, because it's probably got valid repo data in it!

    - name: Check Git project directory for existence
      ansible.builtin.stat:
        path: "{{ projects_dir }}/{{ git_project }}.git"
      register: git_proj_dir

    - name: Create empty {{ git_project }}.git bare repository if missing
      ansible.builtin.command: git init --bare {{ projects_dir }}/{{ git_project }}.git
      when: not (git_proj_dir.stat.isdir is defined and git_proj_dir.stat.isdir)

    - name: Ensure {{ git_project }}.git owned by git
      ansible.builtin.file:
        path: "{{ projects_dir }}/{{ git_project }}.git"
        owner: "{{ git_user }}"
        group: "{{ git_user }}"
        recurse: true
        mode: ug+rwX
        state: directory

    - name: add lab-user to {{ git_user }} group
      ansible.builtin.user:
        name: "{{ username }}"
        groups: "{{ git_user }}"  # group has been created with the user of same name
        append: true
