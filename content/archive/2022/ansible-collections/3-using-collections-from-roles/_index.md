+++
title = "Collections from Roles"
weight = 3
+++

In this exercise you will learn how collections are used from within Ansible roles. One of the cool features of Ansible is to make it easy to reuse existing code. It's very common to write your own roles which rely on existing collections. You will learn in this chapter how to define such a collection dependency and how to use it in your own roles.).

Before we start, let's create a new working directory:

```bash
[student@ansible-1 ~]$ cd
[student@ansible-1 ~]$ mkdir roles-lab
[student@ansible-1 ~]$ cd roles-lab
```

### Define Collection Dependencies

First we'll create a simple role. Start with creating a new role scaffold using the `ansible-galaxy init` command (make sure you changed into your **roles-lab** folder):

```bash
[student@ansible-1 roles-lab]$ ansible-galaxy init --init-path roles selinux_manage_meta
- Role selinux_manage_meta was created successfully
```

Now you have to edit a couple of files. First edit the role metadata in `roles/selinux_manage_meta/meta/main.yml` and append the following lines at the end of the file, don't change anything else:

```yaml
# Collections list
collections:
  - ansible.posix
```

A role without some tasks is not too interesting, go and edit the `roles/selinux_manage_meta/tasks/main.yml` file and replace the content of the file with the following tasks (make sure to keep the YAML start `---` in place):

```yaml
---
# tasks file for selinux_manage_meta
- name: Enable SELinux enforcing mode
  ansible.posix.selinux:
    policy: targeted
    state: "{{ selinux_mode }}"

- name: Enable booleans
  ansible.posix.seboolean:
    name: "{{ item }}"
    state: true
    persistent: true
  loop: "{{ sebooleans_enable }}"

- name: Disable booleans
  ansible.posix.seboolean:
    name: "{{ item }}"
    state: false
    persistent: true
  loop: "{{ sebooleans_disable }}"
```

{{% notice tip %}}
It's always recommended to use the Fully Qualified Collection Name (FQCN) when working with modules.
{{% /notice %}}

Every well-written role should come with sensible defaults, edit the `roles/selinux_manage_meta/defaults/main.yml` and replace the content of the file to define default values for role variables:

```yaml
---
# defaults file for selinux_manage_meta
selinux_mode: enforcing
sebooleans_enable: []
sebooleans_disable: []
```

As a last step clean up the unused folders of the role:

```bash
[student@ansible-1 roles-lab]$ rm -rf roles/selinux_manage_meta/{tests,vars,handlers,files,templates}
```

And you are done with the role! Run `tree` to check everything looks good:

```bash
[student@ansible-1 roles-lab]$ tree
.
└── roles
    └── selinux_manage_meta
        ├── defaults
        │   └── main.yml
        ├── meta
        │   └── main.yml
        ├── README.md
        └── tasks
            └── main.yml
```

We can now test the new role with a basic playbook. Create the `selinux-enforce.yml` file in the **roles-lab** folder with the following content:

```yaml
---
- hosts: node1
  become: true
  vars:
    sebooleans_enable:
      - httpd_can_network_connect
      - httpd_mod_auth_pam
    sebooleans_disable:
      - httpd_enable_cgi
  roles:
    - selinux_manage_meta
```

Run the playbook and check the results:

{{% notice note %}}
Notice we're not using the FQCN for our own role **selinux_manage_meta** since it's not part of a collection.
{{% /notice %}}

```bash
[student@ansible-1 roles-lab]$ ansible-navigator run selinux-enforce.yml -m stdout
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [localhost] ******************************************************************************************************

TASK [Gathering Facts] ************************************************************************************************
ok: [localhost]

TASK [selinux_manage_meta : Enable SELinux enforcing mode] ************************************************************
ok: [localhost]

TASK [selinux_manage_meta : Enable booleans] **************************************************************************
changed: [localhost] => (item=httpd_can_network_connect)
changed: [localhost] => (item=httpd_mod_auth_pam)

TASK [selinux_manage_meta : Disable booleans] *************************************************************************
changed: [localhost] => (item=httpd_can_network_connect)

PLAY RECAP ************************************************************************************************************
localhost                  : ok=4    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

## Takeaways

- Roles can define collection dependencies by populating the `collections` list defined in `meta/main.yml`.

- Modules or Plugins which are part of a Collections can be called from roles using their FQCN directly from the role tasks.
