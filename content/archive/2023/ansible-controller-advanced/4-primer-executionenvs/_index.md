+++
title = "Primer on Execution Environments: Running Jobs in a Cluster"
weight = 4
+++

## Some Housekeeping

Your lab is using a dedicated **Execution Node** you'll learn more about later in the lab. For now and the next chapters we want it out of the picture, please do the following:

* In the web UI, go to **Instance Groups**
* Open the `default` instance group and switch to the **Instances** tab
* Click the slider to the right of the `exec1.<LABID>.internal` instance to disable the instance

## Run a Job

After boot-strapping the controller configuration from bottom up you are ready to start a job in your controller cluster. In one of your controller nodes web UI’s:

* Open the **Templates** view
* Look for the **Install Apache** Template you created with the Playbook
* Run it by clicking the rocket icon.

## Which Instance did actually run the Job?

At first this is not different from a standard controller setup. But as this is a cluster of active controller hybrid nodes, each instance could have run the job. Let's find out.

The most obvious way is to look at the job in question:

* In the **Details** tab of the job look up the **Execution Node**
* If you closed it already or want to check later, navigate to the **Views** menu, open **Jobs** and look up the job in question.

## Ansible Execution Environments Primer

Now that you have investigated how jobs are run in an automation controller cluster it's about time to learn about one of the major new features in Ansible Automation Platform 2: **Execution Environments**!

Before AAP 2 the Automation Platform execution relied on using **bubblewrap** to isolate processes and **Python virtual environments** (venv) to sandbox dependencies. This led to a number of issues like maintaining multiple venv, migrating Ansible content between execution nodes and much more. The concept of execution environments (EE) solves this by using Linux containers.

An EE is a container run from an image that contains everything your Ansible Playbook needs to run. It's basically a control node in a box that can be executed everywhere a Linux container can run. There are ready-made images that contain everything you would expect on an Ansible control node, but you can (and probably will) start to build your own, custom image for your very own requirements at some point.

{{% notice tip %}}
Linux containers are technologies that allow you to package and isolate applications with their entire runtime environment. This makes it easy to move the contained application between environments and nodes while retaining full functionality.
In this lab you'll use the command `podman` later on. Podman is a daemon-less container engine for developing, managing, and running Open Container Initiative (OCI) containers and container images on your Linux System. If you want to learn more, there is a wealth of information on the Internet, you could start [here](http://docs.podman.io/en/latest/Introduction.html) for Podman or [here](https://www.ansible.com/blog/introduction-to-ansible-builder) for execution environments.
{{% /notice %}}

## Execution Environments: A deeper look

As promised let's look a bit deeper into execution environments. In your automation controller web UI, navigate to the **Administration** menu and open **Execution Environments**.

You'll see a list of execution environments with the location of the image, in our case the images are provided locally by your **Private Automation Hub** registry that basically mirrors the EE's in **registry.redhat.io**.

So what happens, when automation controller runs an ad hoc command or Playbook? Let's see...

You should already have your **VS Code** terminal open in another browser tab, if not open https://bastion.\<LABID>.\<SUBDOMAIN>.opentlc.com and do **Terminal**, **New Terminal**. In this terminal:

* SSH into your automation controller node (obviously replace \<LABID> by your value):

  * `ssh autoctl1.<LABID>.internal`

* You should be the **ec2-user** user on your automation controller now, become the **awx** user by running `sudo -u awx -i`

* First let's look for the image, you should have used the **Default execution environment** in your Playbook run which should result in this image (details might be different, there might be other images already):

```bash
$ podman images
REPOSITORY                                                            TAG         IMAGE ID      CREATED      SIZE
pah.<LABID.sandbox589.opentlc.com/ee-supported-rhel8                  latest      4f8a1ce134d8  5 weeks ago  1.32 GB
registry.redhat.io/ansible-automation-platform-22/ee-supported-rhel8  latest      4f8a1ce134d8  5 weeks ago  1.32 GB
registry.redhat.io/ansible-automation-platform-22/ee-minimal-rhel8    latest      9daf978b7c82  5 weeks ago  283 MB
registry.redhat.io/ansible-automation-platform-22/ee-29-rhel8         latest      d56275e986d7  5 weeks ago  802 MB
```

These execution environments were pulled from the registry to be available locally.

Now we want to observe how an EE is run as a container when you execute your automation. `podman ps -w 2` will list running containers, the `-w` option updates the output regularly. Run the command, at first it will show you something like this (no container running):

```raw
CONTAINER ID  IMAGE   COMMAND  CREATED  STATUS  PORTS   NAMES
```

Keep podman running, now it's time to execute some automation. Which should take some time so you can observe what's happening.

* In the web UI run an ad hoc command. Go to **Resources**, **Inventories** and **Lab Inventory**
* In the **Hosts** view select the three hosts.
* Click **Run Command**. Specify the ad hoc command:
  * **Module**: command
  * **Arguments**: sleep 60
  * **Next**
  * **Execution Environment**: Don't specify an EE, the **Default execution environment** will be used.
  * **Next**
  * **Machine Credential**: Lab Credentials
  * Click **Next**
  * Review the summary page and click **Launch**

Now go back to the **VS Code** terminal. You'll see a container is launched to run the ad hoc command and then removed again:

```raw
CONTAINER ID  IMAGE                                                          COMMAND               CREATED         STATUS             PORTS       NAMES
b771649643ea  pah.<LABID>.<SUBDOMAIN>.opentlc.com/ee-supported-rhel8:latest  ssh-agent sh -c t...  34 seconds ago  Up 34 seconds ago              ansible_runner_5
```

{{% notice tip %}}
The `sleep 60` command was only used to keep the container running for some time. Your output will differ slightly.
{{% /notice %}}

{{% notice warning %}}
This will of course only work if the job is run on the controller node you logged in via SSH (autoctl1) but this should be the default.
{{% /notice %}}

* Feel free to relaunch the job again!
* Stop `podman` with `CTRL-C`.
* Logout of automation controller (twice `exit` or `CTRL-D`)

This is how automation controller uses Linux containers to run Ansible automation jobs in their own dedicated environments.
